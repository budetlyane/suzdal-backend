const Application = require('spectron').Application
const path = require('path')

const app = new Application({
  path: './node_modules/.bin/electron',
  args: ['.']
})

describe('App', () => {
  /**
   * Setup
   */
  jest.setTimeout(50 * 1000)
  beforeAll(async () => {
    await app.start()
    await app.client.waitUntilWindowLoaded()
  })
  /**
   * Teardown
   */
  afterAll(async () => {
    await app.stop()
  })

  it('should open a window', async () => {
    expect.assertions(1)
    // act
    const isVisible = await app.browserWindow.isVisible()
    // assert
    expect(isVisible).toBe(true)
  })

  it('should show the loading text', async () => {
    expect.assertions(1)
    // act
    const text = await app.client.getText('h2')
    // assert
    expect(text).toEqual(['Siderus Orion', 'Loading...'])
  })

  it('should add a file', async () => {
    // arrange
    const filePath = path.join(__dirname, '__fixtures__', 'directory-with-file', 'hello.txt')
    // hello.txt has the following hash
    const helloCID = 'QmT78zSuBmuS4z925WZfrqQ1qHaJ56DQaTfyMUF7F8ff5o'
    // act
    await app.client.waitForExist('#add-file-input')
    await app.client.chooseFile('#add-file-input', filePath)
    // assert
    await app.client.waitUntil(async () => {
      const hashes = await app.client.getText('th[scope="row"]')
      return hashes.includes(helloCID)
    }, 5000, 'expected the hash to appear in the storage list')
  })

  it.skip('should download a file', async () => {
    // arrange
    const helloCID = 'QmT78zSuBmuS4z925WZfrqQ1qHaJ56DQaTfyMUF7F8ff5o'
    const rowSelector = `tr[forhash="${helloCID}"]`
    const actionsButtonSelector = `${rowSelector} button`
    const downloadButtonSelector = '#action-menu li[handler="onDownload"]'
    // act
    await app.client.waitForExist(rowSelector)
    const row = await app.client.element(rowSelector)
    await app.client.click(actionsButtonSelector)
    await app.client.click(downloadButtonSelector)
    // TODO choose path for saving
  })

  it('should remove a file', async () => {
    // arrange
    const helloCID = 'QmT78zSuBmuS4z925WZfrqQ1qHaJ56DQaTfyMUF7F8ff5o'
    const rowSelector = `tr[forhash="${helloCID}"]`
    const actionsButtonSelector = `${rowSelector} button`
    const removeButtonSelector = '#action-menu li[handler="onRemove"]'
    // act
    await app.client.waitForExist(rowSelector)
    const row = await app.client.element(rowSelector)
    await app.client.click(actionsButtonSelector)
    await app.client.click(removeButtonSelector)
    // assert
    await app.client.waitUntil(async () => {
      const hashes = await app.client.getText('th[scope="row"]')
      return !hashes.includes(helloCID)
    }, 5000, 'expected the hash to be removed from the storage list')
  })
})

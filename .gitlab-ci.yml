image: electronuserland/builder:wine

cache:
  # Enables cache on Node Modules on the same branch
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/

stages:
  - test
  - pre-release
  - release
  - post-release

.job_template: &install_requirements
  before_script:
    - apt-get -qq update
    - apt-get -qq install -y jq sed gcc-multilib g++-multilib ipfs

test:
  <<: *install_requirements
  stage: test
  script:
    - make lint test_ci
  only:
    changes:
      - package.json
      - src/**/*

variables:
  # Setting this variable will affect all Security templates
  # (SAST, Dependency Scanning, ...)
  SECURE_ANALYZERS_PREFIX: "registry.gitlab.com/gitlab-org/security-products/analyzers"

  DS_DEFAULT_ANALYZERS: "bundler-audit, retire.js, gemnasium, gemnasium-maven, gemnasium-python"
  DS_EXCLUDED_PATHS: "spec, test, tests, tmp"
  DS_MAJOR_VERSION: 2

dependency_scanning:
  stage: test
  script:
    - echo "$CI_JOB_NAME is used for configuration only, and its script should not be executed"
    - exit 1
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
  dependencies: []
  rules:
    - when: never

.ds-analyzer:
  extends: dependency_scanning
  allow_failure: true
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/
  script:
    - /analyzer run

retire-js-dependency_scanning:
  extends: .ds-analyzer
  image:
    name: "$DS_ANALYZER_IMAGE"
  variables:
    DS_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/retire.js:$DS_MAJOR_VERSION"
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $DS_DEFAULT_ANALYZERS =~ /retire.js/
      exists:
        - '{package.json,*/package.json,*/*/package.json}'


build:mac:
  <<: *install_requirements
  stage: pre-release
  script:
    - make prepare_release
    - make build_mac -e NODE_ENV=production
    - make purge_build_directory
  artifacts:
    paths:
    - build
    expire_in: 3 days
  when: manual

build:win:
  <<: *install_requirements
  stage: pre-release
  script:
    - make prepare_release
    - make build_win -e NODE_ENV=production
    - make purge_build_directory
  artifacts:
    paths:
    - build
    expire_in: 3 days
  when: manual

build:win32:
  <<: *install_requirements
  stage: pre-release
  script:
    - make prepare_release
    - make build_win32 -e NODE_ENV=production
    - make purge_build_directory
  artifacts:
    paths:
    - build
    expire_in: 3 days
  when: manual

build:gnu:
  <<: *install_requirements
  stage: pre-release
  script:
    - make prepare_release
    - make build_gnu -e NODE_ENV=production
    - make purge_build_directory
  artifacts:
    paths:
    - build
    expire_in: 3 days
  when: manual


release:stable:
  <<: *install_requirements
  stage: release
  script:
    - ipfs init -profile=server
    - ipfs daemon &
    - sleep 10
    - curl https://meta.siderus.io/ipfs/connect.sh  | bash
    - make prepare_release -e GIT_TAG="$CI_COMMIT_TAG"
    - make release_all -e NODE_ENV=production -e GIT_TAG="$CI_COMMIT_TAG"
    - make purge_build_directory upload_ipfs -e GIT_TAG="$CI_COMMIT_TAG"
  cache: {} # Disable cache
  artifacts:
    paths:
    - build
    expire_in: 1 month
  only:
    refs:
      - tags
    variables:
      - $CI_COMMIT_TAG =~ /^v(\d+\.)?(\d+\.)?(\*|\d+)$/

release:beta:
  <<: *install_requirements
  stage: release
  script:
    - ipfs init -profile=server
    - ipfs daemon &
    - sleep 10
    - curl https://meta.siderus.io/ipfs/connect.sh  | bash
    - make prepare_release -e GIT_TAG="$CI_COMMIT_TAG"
    - make release_all -e NODE_ENV=production -e GIT_TAG="$CI_COMMIT_TAG"
    - make purge_build_directory upload_ipfs -e GIT_TAG="$CI_COMMIT_TAG"
  cache: {} # Disable cache
  artifacts:
    paths:
    - build
    expire_in: 1 month
  only:
    refs:
      - tags
    variables:
      - $CI_COMMIT_TAG =~ /^v(\d+\.)?(\d+\.)?(\*|\d+)-beta$/

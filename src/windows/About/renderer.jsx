import React from 'react'
import ReactDom from 'react-dom'
import { shell, ipcRenderer } from 'electron'
import styled from 'styled-components'

import OrionLogo from '../../../assets/logo-white.svg'
import SiderusLogo from '../../../assets/siderus-icon-white.svg'
import pjson from '../../../package.json'

const Window = styled.div`
  font: caption;
  font-size: 0.90em;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 24px;
  color: white;
  -webkit-app-region: drag;
`
const Orion = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 10px;
`

const LinkHover = styled.span`
  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`

class AboutWindow extends React.Component {
  state = {
    text: 'Loading',
    percentage: 0,
    skipLatestUIFetch: false
  }

  useLocalUI = () => {
    this.setState({ skipLatestUIFetch: true })
    ipcRenderer.send('message', {
      method: 'shell/useLocalUI'
    })
  }

  componentDidMount () {
    ipcRenderer.on('set-progress', (event, data) => this.setState(data))
  }

  componentWillUnmount () {
    ipcRenderer.removeAllListeners('set-progress')
  }

  render () {
    const {
      skipLatestUIFetch,
      percentage, text
    } = this.state

    return (
      <Window>
        <Orion>
          <OrionLogo height='150px' color="white" />
        </Orion>
        <p>
          <b>App version</b>: {pjson.version}<br />
          <b>IPFS version</b>: {pjson.ipfsVersion}<br />
          <br />
          <div onClick={event => shell.openExternal('http://suz-dal.net')}>
            Website: <LinkHover>http://suz-dal.net/</LinkHover>
          </div>
          <div onClick={event => shell.openExternal('http://budetlyane.ru')}>
            2019 <LinkHover>Будетляне</LinkHover>
          </div>
        </p>
      </Window>
    )
  }
}

ReactDom.render(<AboutWindow />, document.querySelector('#host'))

import path from 'path'
import url from 'url'

import { BrowserWindow } from 'electron'

function create () {
  // Create the browser modal window.
  let thisWindow = new BrowserWindow({
    width: 500,
    minWidth: 500,
    height: 310,
    minHeight: 310,

    frame: false,
    maximizable: false,
    resizable: false,
    fullscreenable: false,
    icon: path.join(__dirname, '../../../assets/icon.png'),

    show: false,
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, '../../lib/report/preload.js')
    }
  })

  // Show the menu only on the main window
  thisWindow.setMenu(null)

  // Show the window only when ready
  thisWindow.once('ready-to-show', () => {
    setTimeout(() => { thisWindow.show() }, 500)
  })

  // Load the index.html of the app.
  thisWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  thisWindow.on('closed', () => {
    // Removes  the progress bar
    thisWindow.setProgressBar(-1)
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    thisWindow = null
  })

  return thisWindow
}

module.exports = {
  create
}

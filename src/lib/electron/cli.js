import { resolve as pathResolve } from 'path'

/**
 * if the app was started with the --add parameter
 * Return a boolean promise to check for CLI
 */
export function isAnAddRequest () {
  return Promise.resolve(process.argv.indexOf('--add') !== -1)
}

// Return the log level. Do we _really_ need docs for this? :P
export function getLogLevel () {
  return 'debug'
}

/**
 * if the app was started with the --add parameter provides the value as a
 * resolved path.
 *
 * Return a promise with the array of all the files to add
 */
export function getAddArgValues () {
  const addIndex = process.argv.indexOf('--add')
  const filesToAdd = process.argv.splice(addIndex + 1).map(file => pathResolve(file))
  return Promise.resolve(filesToAdd)
}

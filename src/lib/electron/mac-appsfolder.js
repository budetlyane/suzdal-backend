import { app, dialog } from 'electron'
import pjson from '../../../package.json'

/*
 * checkMacApplicationInFolder works on macOS and checks if the application
 * is currently installed in /Applications folder. This ensures that the app
 * is installed and working properly.
 */
export function checkMacApplicationInFolder () {
  if (pjson.env === 'development') return
  if (process.platform !== 'darwin') return

  if (app.isInApplicationsFolder()) return
  let question = 'The application is not installed.'
  question += '\n\nWould you like to install Suzdal?'

  const btnId = dialog.showMessageBox({
    type: 'info',
    message: question,
    buttons: ['No', 'Yes'],
    cancelId: 0,
    defaultId: 1
  })

  if (btnId === 0) return

  app.moveToApplicationsFolder()
}

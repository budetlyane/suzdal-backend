import rootDir from 'app-root-dir'
import pjson from '../../../package.json'
import { getIpfsApiWithAddress } from '../ipfs/api'
import { addFileLocallyFromPath } from '../ipfs/files'
import { getNetworkPeers } from './peers'
import { connectTo, addBootstrapAddr } from '../ipfs/peers'
import { promiseSleep } from '../utils'
import isOnline from 'is-online'
import log from 'electron-log'
import tar from 'tar'
import { dirSync as tmpDirSync } from 'tmp'
import { copyFileSync, readFileSync, writeFile } from 'fs'
/**
 * getLatestUIPath will provide the latest UI version or if not possible, the
 * current supported.
 *
 * @returns {Promise<string>}
 */
export function getLatestUIPath () {
  return isOnline().then((isConn) => {
    if (!isConn) {
      return Promise.resolve(pjson.ui.cid)
    }

    return getIpfsApiWithAddress(global.IPFS_MULTIADDR_API)
      .then(ipfs_client => ipfs_client.dns(pjson.ui.ipns))
      .then(cid => {
        // if the new CID is not available return the default one
        if (!cid) return Promise.resolve(pjson.ui.cid)
        return Promise.resolve(cid)
      })
  })
}

/**
 * preloadUICID will fetch a specific DAG for the UI. This should speed up
 * the download of the content
 *
 * @returns {Promise<string>}
 */
export function preloadUICID (cid) {
  return isOnline().then((isConn) => {
    if (!isConn) {
      return Promise.resolve()
    }

    return getIpfsApiWithAddress(global.IPFS_MULTIADDR_API)
      .then(ipfs_client => ipfs_client.pin.add(cid, { recursive: true }))
      // write new UI cid in package.json and pin it to IPFS instead of old one
      .then(() => {
        log.info('[App] Latest SPA version pinned: ', cid)
        const packageRaw = readFileSync(global.PACKAGE_JSON, { encoding: 'utf8' })
        let packageJson = JSON.parse(packageRaw)

        // slice convert "/ipfs/<hash>" to just "<hash>"
        packageJson.ui.cid = cid.slice(-46)

        // Return a promise that writes the package.json back in the file
        writeFile(global.PACKAGE_JSON, JSON.stringify(packageJson, null, 2), function (err) {
          if (err) return Promise.reject(err)
          log.info('[App] Latest SPA version writed to package.json: ', cid.slice(-46))
          Promise.resolve(cid)
        })
      })
      .then(() => Promise.resolve(cid.slice(-46)))
      // .then(() => ) // Clean the output
  })
}

/**
 * addIncludedUI is designed to add to the UI that is included in the app
 * to the local node in order to offer offline interface.
 */
export function addIncludedUI () {
  // the UI pkg has to be copied out from ASAR package
  const uipkgPath = `${rootDir.get()}/assets/ui.tar`
  const uiDirPath = tmpDirSync({ keep: true }).name
  const tmpPkgPath = `${uiDirPath}/ui.tar`
  const destUIPath = tmpDirSync({ keep: true }).name

  log.debug(`[App] Decompressing UI from ${uipkgPath}, to ${destUIPath}`)
  copyFileSync(uipkgPath, tmpPkgPath)

  return tar.extract({ cwd: destUIPath, file: tmpPkgPath }).then(() => {
    log.debug(`[App] Done decompressing`)

    const options = { recursive: true, pin: false, wrapWithDirectory: false }
    return addFileLocallyFromPath(`${destUIPath}`, options)
      .then((result) => {
        log.debug(`[App] Added UI in IPFS: ${result[result.length - 1].hash}`)
      })
  })
}

// loadLatestUI is a chain of promises to ensure connection and preload the
// UI from the latest version available via IPNS/DNSLink
export function loadLatestUI (loadingWindow) {
  log.debug('[App] Fetching edge nodes of Suzdal Network')
  // Fetch the peers to connect to
  loadingWindow.webContents.send('set-progress', {
    text: 'Загрузка данных из трактовочной сети', percentage: 50
  })

  return getNetworkPeers()
    .catch(err => {
      log.warn('[App] Error while fetching the Suzdal nodes: ', err)
      return Promise.resolve([])
    })
    .then(peers => {
      log.debug('[App] Connecting to Suzdal Network')
      // Connect to the peers
      loadingWindow.webContents.send('set-progress', {
        text: 'Подключение к трактовочной сети', percentage: 55
      })
      let connectPromises = peers.map(addr => { return connectTo(addr, global.IPFS_MULTIADDR_API) })
      let bootstrapPromises = peers.map(addr => { return addBootstrapAddr(addr, global.IPFS_MULTIADDR_API) })
      return Promise.race(connectPromises.concat(bootstrapPromises))
    })
    // Sleep 1 second due to connections to the network
    .then(() => promiseSleep(1 * 1000))
    // Download/pre-fetch latest SPA.
    .then(() => {
      loadingWindow.webContents.send('set-progress', {
        text: 'Проверка обновлений', percentage: 70
      })
      log.debug('[App] Fetching CID for latest SPA')
    })
    // Downloads the SPA interaface to ensure the window is not empty
    .then(getLatestUIPath)
    .then((cid) => {
      log.info('[App] Latest SPA version:', cid)
      loadingWindow.webContents.send('set-progress', {
        text: 'Новая версия интерфейса найдена. Скачиваем из IPFS', percentage: 80
      })
      log.debug('[App] Fetching DAG for:', cid)
      return preloadUICID(cid)
    })
}

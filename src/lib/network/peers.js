import request from 'request-promise-native'
import pjson from '../../../package.json'
import log from 'electron-log'
import isOnline from 'is-online'

/**
 * getNetworkPeers returns a Promise that will download and return a list of
 * multiaddress (as str) of IPFS nodes from Siderus Network.
 *
 * @returns {Promise<>}
 */
export function getNetworkPeers () {
  return isOnline().then((connection) => {
    // If there is no connection, return an empty array
    if (!connection) {
      return Promise.resolve([])
    }

    return request({
      uri: 'http://suz-dal.net/ipfs/peers.txt',
      headers: { 'User-Agent': `Suzdal/${pjson.version}-shell` }
    }).then(res => {
      let peers
      // split the file by endlines
      peers = res.split(/\r?\n/)
      // remove empty lines
      peers = peers.filter(el => el.length > 0)
      return Promise.resolve(peers)
    }).catch(log.error)
  })
}

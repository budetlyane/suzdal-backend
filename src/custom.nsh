!macro customInstall
      ; Writing the Windows Context menu in the registry. This will add right
      ; click capabiliteis. HKCU = Current user, HKLM = Local Machine, all users

      ; Adding it for any file
      WriteRegStr HKLM "Software\Classes\*\shell\Suzdal App\command" "" `"$INSTDIR\Suzdal App.exe" --add "%1"`
      WriteRegStr HKLM "Software\Classes\*\shell\Suzdal App" "Icon" "$INSTDIR/Suzdal App.exe"
      WriteRegStr HKLM "Software\Classes\*\shell\Suzdal App" "" "Add to Suzdal App"

      ; Adding the option for Directories
      WriteRegStr HKLM "Software\Classes\Directory\shell\Suzdal App\command" "" `"$INSTDIR\Suzdal App.exe" --add "%1"`
      WriteRegStr HKLM "Software\Classes\Directory\shell\Suzdal App" "Icon" "$INSTDIR/Suzdal App.exe"
      WriteRegStr HKLM "Software\Classes\Directory\shell\Suzdal App" "" "Add to Suzdal App"

      ; Adding Options for background Directories
      ; WriteRegStr HKLM "Software\Classes\Directory\background\shell\Suzdal App\command" "" `"$INSTDIR\Suzdal App.exe" --add "%V"`
      ; WriteRegStr HKLM "Software\Classes\Directory\background\shell\Suzdal App" "Icon" "$INSTDIR/Suzdal App.exe"
      ; WriteRegStr HKLM "Software\Classes\Directory\background\shell\Suzdal App" "" "Add to Suzdal App"
!macroend

!macro customUnInstall
      ; This will delete the keys saved before for right-click capabilities
      DeleteRegKey HKLM "Software\Classes\*\shell\Suzdal App"
      DeleteRegKey HKLM "Software\Classes\Directory\shell\Suzdal App"
      DeleteRegKey HKLM "Software\Classes\Directory\background\shell\Suzdal App"
      ; Delete the AppData directory
      ; RMDir /r "$APPDATA\${APP_FILENAME}"
!macroend
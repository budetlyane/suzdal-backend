import { app, dialog } from 'electron'
import rootDir from 'app-root-dir'
import { join as pathJoin } from 'path'
import isOnline from 'is-online'
import log from 'electron-log'
//
import './menu'
import LoadingWindow from './windows/Loading/window'
import MainWindow from './windows/Main/window'

import './lib/report/node'
import { checkMacApplicationInFolder } from './lib/electron/mac-appsfolder'
import { checkForUpdates } from './lib/electron/autoupdate'
import setupTrayIcon from './lib/electron/tray-icon'
import blockMultipleInstances from './lib/electron/multiple-instances'
import { getLogLevel, isAnAddRequest, getAddArgValues } from './lib/electron/cli'

import {
  ensuresIPFSInitialised,
  ensureRepoMigrated,
  ensureDaemonConfigured,
  startIPFSDaemon,
  shouldStartDaemon,
  ensurePortNotTaken
} from './daemon'

import { addFileLocallyFromPath } from './lib/ipfs/files'
import { loadLatestUI, addIncludedUI } from './lib/network/preload'
import { promiseSleep } from './lib/utils'
import { waitForAPIReady } from './lib/ipfs/api'
import settings from 'electron-settings'

// This will set up ipc events for SPA<-->Electron messages
import './lib/electron/ipc'

import pjson from '../package.json'

// Set logging level
log.transports.console.level = getLogLevel()
log.transports.file.file = pathJoin(app.getPath('userData'), 'log.txt')

// A little space for IPFS processes
global.IPFS_PROCESS = null

// Sets default values for IPFS configurations
if (process.platform === 'win32') {
  global.IPFS_BINARY_PATH = `${rootDir.get()}/go-ipfs/ipfs.exe`
  global.REPO_MIGRATIONS_BINARY_PATH = `${rootDir.get()}/fs-repo-migrations/fs-repo-migrations.exe`
  global.IPFS_CLUSTER_SERVICE_BINARY_PATH = `${rootDir.get()}/ipfs-cluster-service/ipfs-cluster-service.exe`
  global.IPFS_CLUSTER_FOLLOW_BINARY_PATH = `${rootDir.get()}/ipfs-cluster-follow/ipfs-cluster-follow.exe`
} else {
  global.IPFS_BINARY_PATH = `${rootDir.get()}/go-ipfs/ipfs`
  global.REPO_MIGRATIONS_BINARY_PATH = `${rootDir.get()}/fs-repo-migrations/fs-repo-migrations`
  global.IPFS_CLUSTER_SERVICE_BINARY_PATH = `${rootDir.get()}/ipfs-cluster-service/ipfs-cluster-service`
  global.IPFS_CLUSTER_FOLLOW_BINARY_PATH = `${rootDir.get()}/ipfs-cluster-follow/ipfs-cluster-follow`
}

global.PACKAGE_JSON = `${rootDir.get()}/package.json`
global.IPFS_MULTIADDR_API = '/ip4/127.0.0.1/tcp/5001'
global.IPFS_MULTIADDR_GATEWAY = '/ip4/127.0.0.1/tcp/8080'
global.IPFS_MULTIADDR_SWARM = ['/ip4/0.0.0.0/tcp/4001', '/ip6/::/tcp/4001']
// set the Access-Control-Allow-Origin headers for API responses
global.IPFS_API_CORS_POLICY = ['*']
// Used to point to the right IPFS repo & conf
global.IPFS_REPO_PATH = settings.get('IPFS_REPO_PATH') || pathJoin(app.getPath('userData'), 'ipfs-repo')
global.UI_HTTP_ADDRESS = `http://127.0.0.1:8080/ipns/${pjson.ui.ipns}/#/home`
global.UI_SKIP_FETCHING_LATEST = false

// Tray Icon is initiated here to prevet GC from deleting it
let appTray

app.on('ready', () => {
  log.info('[App] Ready')

  isOnline().then((conn) => {
    if (!conn) {
      log.warn('[App] No internet connection detected')
      global.UI_HTTP_ADDRESS = `http://127.0.0.1:8080/ipfs/${pjson.ui.cid}/#/home`
    }
  })

  // Check that the app is installed or not
  checkMacApplicationInFolder()
  isAnAddRequest().then((isIt) => {
    // If it is a CLI request to add content
    if (isIt) {
      return startCLI()
    }

    // Block multiple instances of the app
    blockMultipleInstances(app)

    // Set up the Tray Icon to run in backgorund on Windows and Linux
    appTray = setupTrayIcon(app)

    // Start the daemon and show the mainWindow
    return startApp()
  })

  // Check for updates
  checkForUpdates()
})

app.on('will-quit', () => {
  if (app !== undefined && app.mainWindow !== null) {
    app.mainWindow.close()
    app.mainWindow = null
  }

  // Kill IPFS process after the windows have been closed and before the app is
  // fully terminated
  if (global.IPFS_PROCESS) {
    global.IPFS_PROCESS.kill()
  }
})

app.on('activate', () => {
  log.info('[App] Active')
  // Re-create the window in the app when the
  // dock icon is clicked and there are no other windows open.

  // If the app is not ready skip the activation
  if (!app.isReady()) {
    return
  }

  // If there is no main window, recreate it or show the existing one
  if (app.mainWindow) {
    app.mainWindow.show()
  } else {
    app.mainWindow = MainWindow.create(app)
  }
})

function prepareAndStartDaemon (loadingWindow = null) {
  log.info('[prepare] Preparing and starting the daemon')
  if (loadingWindow) {
    loadingWindow.webContents.send('set-progress', {
      text: 'Проверка инициализации хранилища IPFS...', percentage: 8
    })
    loadingWindow.setProgressBar(0.8)
  }

  return ensurePortNotTaken()
    .then(ensuresIPFSInitialised)
    .then(() => {
      log.info('[prepare] Checking the IPFS repository')
      if (loadingWindow) {
        loadingWindow.webContents.send('set-progress', {
          text: 'Проверка хранилища IPFS...', percentage: 10
        })
        loadingWindow.setProgressBar(0.1)
      }
    })
    .then(ensureRepoMigrated)
    .then(() => {
      log.info('[prepare] Configuring IPFS daemon')
      if (loadingWindow) {
        loadingWindow.webContents.send('set-progress', {
          text: 'Настройка IPFS...', percentage: 20
        })
        loadingWindow.setProgressBar(0.2)
      }
    })
    // Then change the json of the configuration file
    .then(ensureDaemonConfigured)
    .then(() => {
      log.info('[prepare] IPFS Daemon: Starting')
      if (loadingWindow) {
        loadingWindow.webContents.send('set-progress', {
          text: 'Запуск IPFS...', percentage: 30
        })
        loadingWindow.setProgressBar(0.3)
      }
    })
    .then(startIPFSDaemon)
    .then(process => {
      global.IPFS_PROCESS = process
    })
    .then(() => {
      log.info('Waiting for the API to be ready')
      if (loadingWindow) {
        loadingWindow.webContents.send('set-progress', {
          text: 'Ожидание IPFS API ...', percentage: 40
        })
        loadingWindow.setProgressBar(0.4)
      }
    })
    // Sleep 5 seconds for the daemon
    .then(() => promiseSleep(5 * 1000))
    // we need the api to be ready because we serve the SPA from the gateway
    .then(waitForAPIReady)
    .then(() => {
      log.info('[prepare] IPFS Daemon: Started')
    })
    // Sleep 1 seconds for the daemon
    .then(() => promiseSleep(1 * 1000))
    .catch((err) => {
      loadingWindow.webContents.send('set-progress', {
        text: `Ошибка: ${err}`, percentage: 8
      })
      loadingWindow.setProgressBar(0.8)
      log.error('[prepare] Error preparing and starting the daemon', err)
    })
}

/**
 * startApp will start the app normally. It will do:
 * 0. Show the UI for the loading window
 * 1. Check if we need to start the daemon, and in case start it
 * 2. Pre fetch the SPA DAG to speed up a little the process of retreiving it
 * 3. Prepare and then show the main window
 */
function startApp () {
  log.info('[App] Starting daemon connections')
  // Create a Loading Window
  const loadingWindow = LoadingWindow.create()
  // when it is ready to show, starts loading the app
  loadingWindow.on('ready-to-show', () => {
    loadingWindow.webContents.send('set-progress', {
      text: 'Запуск приложения...', percentage: 0
    })
    loadingWindow.setProgressBar(0)

    return shouldStartDaemon()
      .then((shouldStart) => {
        if (!shouldStart) return Promise.resolve()

        // Configure & Start the daemon in case
        return prepareAndStartDaemon(loadingWindow)// ends with 40%
      })
      .then(() => {
        log.debug('[App] Adding included interface')
        // Fetch the peers to connect to
        loadingWindow.webContents.send('set-progress', {
          text: 'Загрузка текущей версии приложения', percentage: 45
        })
        loadingWindow.setProgressBar(0.45)
        return addIncludedUI()
      })
      // Check if we are connected to the internet, and if so, load the latest UI
      // if we are not connected, skip the fetching/connecting
      .then(isOnline)
      .then((isConnected) => {
        if (!isConnected) {
          log.warn('[App] Unable to fetch latest UI version')
          return Promise.resolve()
        }

        if (global.UI_SKIP_FETCHING_LATEST) {
          log.warn('[App] User requested to skip loading latest UI version')
          return Promise.resolve()
        }

        return loadLatestUI(loadingWindow) // from 50% to 80%
      })
      .then((cid) => {
        global.UI_HTTP_ADDRESS = `http://127.0.0.1:8080/ipfs/${cid}/#/home`
        log.info(`[App] UI_HTTP_ADDRESS set to ${cid}`)
        loadingWindow.webContents.send('set-progress', {
          text: 'Загрузка новой версии приложения...', percentage: 90
        })
        loadingWindow.setProgressBar(0.9)
        log.info('[App] Loading the Main Window')
        // Create the main window
        app.mainWindow = MainWindow.create(app)
        // Close the loading window when the main one is ready
        app.mainWindow.on('ready-to-show', () => {
          setTimeout(() => { loadingWindow.close() }, 500)
          log.info('[App] The SPA is ready')
          loadingWindow.setProgressBar(-1)
        })
      })
      .catch((err) => {
        log.warn('[App] Failed to start:', err)
        loadingWindow.webContents.send('set-progress', {
          text: 'Ошибка. Проверьте логи и перезапустите приложение', percentage: 5
        })
        loadingWindow.setProgressBar(0.1)
        dialog.showMessageBox({
          type: 'error',
          message: `Пожалуйста, перезапустите приложение. Ошибка: ${err}`
        })
        app.quit()
      })
  })
}

/**
 * startCLI will start the app in CLI mode. It will do:
 * 0. ~~Check if the CLI parameters were passed correctly.~~ (not needed for now)
 * 1. Check if we need to start the daemon, and in case start it
 * 2. Check what to do (ex: add a new file)
 * 3. Add the file.
 * 4. Quit the daemon if it was started.
 */
function startCLI () {
  log.info('[CLI] Starting Suzdal in CLI mode')

  // TODO: if we add more actions than just 'add' we need to check the params here
  //       before starting and/or connecting to the daemon.
  return shouldStartDaemon()
    .then((shouldStart) => {
      if (!shouldStart) return Promise.resolve()

      // Configure & Start the daemon in case
      return prepareAndStartDaemon()
    })
    .then(getAddArgValues)
    .then(objPaths => {
      log.info(`[CLI] Adding files: ${objPaths}`)
      return Promise.all(
        objPaths.map(x => addFileLocallyFromPath(x)
          .then((result) => {
            log.info(`[CLI] File added: ${x}`)
            log.debug(`[CLI] File Add result for ${x}: ${result}`)
          })
        ))
        .then(() => {
          dialog.showMessageBox({
            type: 'info',
            message: `File/s added correctly to Suzdal`
          })
        })
        // Sleep for 5 seconds for the Daemon.
        .then(() => promiseSleep(5 * 1000))
        .then(() => app.quit())
    })
    .catch((err) => {
      log.error('[CLI] Errro adding a file', err)
      dialog.showMessageBox({
        type: 'error',
        message: `An error occurred: ${err}`
      })
      app.quit()
    })
}
